from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import Forum, Lowongan

# Create your views here.
response = {}
'''with login
def index(request):
    if 'ver_id' not in request.session.keys():
        return HttpResponseRedirect(reverse('login:index'))
    else:
        kode_identitas = request.session['ver_id']
        try:
            forum = Forum.objects.get(kode_identitas=kode_identitas)
        except Exception:
            forum = Forum()
            forum.kode_identitas = kode_identitas
            forum.save()
        lowongan = Lowongan.objects.filter(forum=forum)
        paginator = Paginator(lowongan, 1)
        page = request.GET.get('page')
        try:
            data = paginator.page(page)
        except PageNotAnInteger:
            data = paginator.page(1)
        except EmptyPage:
            data = paginator.page(paginator.num_pages)
        response['lowongan'] = data
        html = 'user_forum/user_forum.html'
        return render(request, html, response)
    '''
'''without login'''
def index(request):
        kode_identitas = 1
        try:
            forum = Forum.objects.get(kode_identitas=kode_identitas)
        except Exception:
            forum = Forum()
            forum.kode_identitas = kode_identitas
            forum.save()
        lowongan = Lowongan.objects.filter(forum=forum)
        paginator = Paginator(lowongan, 1)
        page = request.GET.get('page')
        try:
            data = paginator.page(page)
        except PageNotAnInteger:
            data = paginator.page(1)
        except EmptyPage:
            data = paginator.page(paginator.num_pages)
        response['lowongan'] = data
        html = 'user_forum/user_forum.html'
        return render(request, html, response)

'''with login
@csrf_exempt
def add_lowongan(request):
    if(request.method == "POST"):
        kode_identitas = request.session['ver_id']
        forum = Forum.objects.get(kode_identitas=kode_identitas)
        lowongan = lowongan(forum)
        lowongan.lowongan(request.POST['input'])
        lowongan.save()
        data = {'input' : request.POST['input']}
        return JsonResponse(data)
    else:
        return HttpResponseRedirect('/forum/')
    '''

'''without login'''
@csrf_exempt
def add_lowongan(request):
    if(request.method == "POST"):
        data = request.POST['input']
        kode_identitas = 1
        forum = Forum.objects.get(kode_identitas=kode_identitas)
        lowongan = Lowongan(forum=forum, lowongan=data).save()
        response = {'input' : data}
        return JsonResponse(response)
    else:
        return HttpResponseRedirect('/forum/')
