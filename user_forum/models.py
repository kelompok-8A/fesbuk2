from django.db import models

# Create your models here.
class Forum(models.Model):
    kode_identitas = models.CharField('Kode Identitas', max_length=50, primary_key=True)
    created_at = models.DateTimeField(auto_now_add=True)

class Lowongan(models.Model):
    forum = models.ForeignKey(Forum)
    lowongan = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
