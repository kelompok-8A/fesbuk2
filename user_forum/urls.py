from django.conf.urls import url
from .views import index, add_lowongan

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'add-lowongan', add_lowongan, name="add-lowongan"),
]
