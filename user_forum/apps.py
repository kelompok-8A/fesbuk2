from django.apps import AppConfig


class UserForumConfig(AppConfig):
    name = 'user_forum'
