from django.shortcuts import render
from django.http import HttpResponseRedirect
from login.models import Company

response = {}

def index(request):
    response['author'] = "Kelompok 8"
    html = "profile.html"
    company = Company.objects.get(company_id = 13604028)
    response['company'] = company
    return render(request, html, response)
