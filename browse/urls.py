from django.conf.urls import url
from .views import index, get_forum_browse_data

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^browse/forum/$', get_forum_browse_data, name='get_forum_browse_data'),
]
