$(document).ready(function() {

});

const load_forum_data = function(page, callback) {
    $.get('browse/forum/?page=' + page, function(response) {
        forum_browse_data = response.forum_browse_data;

        for (var i = 0; i < forum_browse_data.length; i++) {
            forum = forum_browse_data[i];

            $('.company-list').append(
                '<div class="company-item">' +
                    '<h3>' + forum.name + '</h3>' +
                    '<p>' + forum.post + '</p>' +
                '</div>'
            );
        }

        callback();
    });
}

$(window).on('scroll', function() {
    is_loading = $(document).data('fesbuk2.browse.is_loading');

    if(!is_loading && $(window).scrollTop() > $(document).height() - $(window).height() - 500) {
        $(document).data('fesbuk2.browse.is_loading', true);

        curr_page = $(document).data('fesbuk2.browse.page');
        if (!curr_page) {
            curr_page = 0;
        }
        new_page = curr_page + 1;
        load_forum_data(new_page, function() {
            $(document).data('fesbuk2.browse.page', new_page);
            $(document).data('fesbuk2.browse.is_loading', false);
        });

    }
}).scroll();