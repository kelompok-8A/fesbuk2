from django.shortcuts import render
from django.http import JsonResponse

from user_forum.models import Forum, Lowongan

# Create your views here.

FORUM_DATA_COUNT_PER_PAGE = 10

response = {}

def index(request):
    html = 'browse/browse.html'

    return render(request, html, response)

def get_forum_browse_data(request):
    page = int(request.GET.get('page', '1'))

    offset = FORUM_DATA_COUNT_PER_PAGE * (page-1)
    limit = FORUM_DATA_COUNT_PER_PAGE

    lowongan_list = Lowongan.objects.order_by('-created_at')[offset:limit]

    dummy_data = [
         {
            'name' : 'BukaHati',
            'post' : 'Mencari programmer handal'
        },{
            'name' : 'CariJodoh',
            'post' : 'Mencari front-end developer, gaji min: 5jt'
        },{
            'name' : 'Bram',
            'post' : 'Eh ini bukan sosmed ya?'
        },{
            'name' : 'LingIn',
            'post' : 'Plagiat uy'
        },{
            'name' : 'Realtech Montreal',
            'post' : 'Job vacancies for<br>Technology Expert: $1500<br>Vacation Expert: $3500'
        },{
            'name' : 'Company Name',
            'post' : 'Job Vacancy'
        },{
            'name' : 'BukaHati',
            'post' : 'Mencari CTO handal'
        },{
            'name' : 'BukaHati',
            'post' : 'Mencari CEO handal'
        },{
            'name' : 'Bram',
            'post' : 'Halo semua'
        }
    ]

    forum_browse_data = []
    if(page <= 2):
        forum_browse_data+= dummy_data

    for lowongan in lowongan_list:
        forum_browse_data.append({
            'name' : lowongan.forum.kode_identitas,
            'post' : lowongan.lowongan
        })

    data = {
        'forum_browse_data' : forum_browse_data
    }

    return JsonResponse(data)
