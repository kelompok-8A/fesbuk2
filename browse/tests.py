from django.test import TestCase
from django.test import Client

from user_forum.models import Forum, Lowongan

# Create your tests here.

class BrowseUnitTest(TestCase):

    def test_browse_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_browse_forum_data_success(self):
        forum = Forum.objects.create(kode_identitas='sample id')
        lowongan = Lowongan.objects.create(forum=forum, lowongan='sample text')

        response = Client().get('/browse/forum/?page=1')
        html_response = response.content.decode('utf8')

        self.assertIn('sample id', html_response)
        self.assertIn('sample text', html_response)