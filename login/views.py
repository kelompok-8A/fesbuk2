from django.shortcuts import render
from .linkedin_helper import *
from django.http import *
from django.urls import reverse
# Create your views here.
response = {}
def index(request):
    return render(request, 'landing_page.html', response)

def get_data_session(request):
    if get_data_user(request, 'user_login'):
        response['author'] = get_data_user(request, 'user_login')

def set_data_for_session(request):
    response['author'] = get_data_user(request, 'user_login')
    response['ver_id'] = request.session['ver_id']

def dashboard(request):
    print ("#==> dashboard")

    if not 'user_login' in request.session.keys():
        return HttpResponseRedirect(reverse('login:login'))
    else:
        set_data_for_session(request)
        kode_identitas = get_data_user(request, 'ver_id')

        html = 'login/profile_sementara.html'
        return render(request, html, response)

def create_company(request):
    if request.method == 'POST':
        company_id = request.POST['company_id']
        name = request.POST['name']
        company_type = request.POST['company_type']
        specialties = request.POST['specialties']
        website_url = request.POST['website_url']
        company = Company()
        company.company_id = company_id
        company.name = name
        company.company_type = company_type
        company.website_url = website_url
        company.specialties = specialties
        company.save()
    return HttpResponseRedirect(reverse('profile:index'))