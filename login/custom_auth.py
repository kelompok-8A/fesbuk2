from django.contrib import messages
from django.http import HttpResponseRedirect
from django.urls import reverse

from .linkedin_helper import get_access_token, verify_user

#authentication
def auth_login(request):
    if request.method == "GET":
        #call csui_helper
        access_token = get_access_token()
        if access_token is not None:
            ver_user = verify_user(access_token)
            ver_id = ver_user['id']
            # set session
            request.session['access_token'] = ver_user['access_token']
            request.session['ver_id'] = ver_id

            messages.success(request, "Anda berhasil login")
        else:
            messages.error(request, "Login dengan Linkedin gagal")
    return HttpResponseRedirect(reverse('login:index'))

def auth_logout(request):
    request.session.flush() # menghapus semua session
    messages.info(request, "Anda berhasil logout. Semua session Anda sudah dihapus")
    return HttpResponseRedirect(reverse('login:index'))
