import requests

API_AUTH_CODE = "https://www.linkedin.com/oauth/v2/authorization"
API_VERIFY_USER = "https://www.linkedin.com/oauth/v2/accessToken"

def get_access_token():
    try:
        redirect_uri = ""
        state = ""
        payload = {"response_type": "code", "client_id": get_client_id(), "redirect_uri": redirect_uri, "state": state}
        headers = {
            'authorization': "Basic WDN6TmtGbWVwa2RBNDdBU05NRFpSWDNaOWdxU1UxTHd5d3U1V2VwRzpCRVFXQW43RDl6a2k3NEZ0bkNpWVhIRk50Ymg3eXlNWmFuNnlvMU1uaUdSVWNGWnhkQnBobUU5TUxuVHZiTTEzM1dsUnBwTHJoTXBkYktqTjBxcU9OaHlTNGl2Z0doczB0OVhlQ3M0Ym1JeUJLMldwbnZYTXE4VU5yTEFEMDNZeA==",
            'cache-control': "no-cache",
            'content-type': "application/x-www-form-urlencoded"
        }
        response = requests.get(API_AUTH_CODE, params=payload, headers=headers)
        print(response)
        return response.json()["code"]
    except Exception as e:
        return None
        # raise Exception("username atau password sso salah, input : [{}, {}]".format(username, password,))

def get_client_id():
    client_id = '817xevfu0pqzc9'
    return client_id

def get_client_secret():
	client_secret = 'xtl4Ur3VkKFZl6U1'
	return client_secret

def verify_user(access_token, redirect_uri):
    payload = {
        "grant_type": 'authorization_code',
        "code": access_token,
        "redirect_uri": redirect_uri,
        "client_id": get_client_id(),
        "client_secret": get_client_secret()
    }
    response = requests.get(API_VERIFY_USER, params=payload)
    return response.json()
