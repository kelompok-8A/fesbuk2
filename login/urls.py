from django.conf.urls import url
from .views import *
from .custom_auth import *
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),

    # custom auth
    url(r'^custom_auth_login/$', auth_login, name='auth_login'),
    url(r'^custom_auth_logout/$', auth_logout, name='auth_logout'),
    url(r'^create_company/$', create_company, name='create_company'),
    url(r'^dashboard/$', dashboard, name='dashboard'),

]
