from django.db import models

# Create your models here.
class Company(models.Model):
    company_id = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    company_type = models.CharField(max_length=100)
    website_url = models.URLField(null=True)
    specialties = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add = True)